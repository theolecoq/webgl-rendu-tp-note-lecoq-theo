attribute vec3 position;
attribute vec4 color;

varying vec4 vColor;

uniform mat4 perspective;
uniform mat4 rotation;
uniform mat4 translation;
uniform mat4 zoom;

void main() {
    gl_Position = perspective * translation * rotation * zoom * vec4(position, 1);
    gl_PointSize = 1.0;
    vColor = vec4(color, 1.0);
}