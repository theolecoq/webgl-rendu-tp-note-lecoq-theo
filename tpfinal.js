function loadText(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.overrideMimeType("text/plain");
    xhr.send(null);
    if (xhr.status === 200)
        return xhr.responseText;
    else {
        return null;
    }
}

// variables globales du programme;
var canvas;
var gl; //contexte
var program; //shader program
var attribPos; //attribute position
var pointSize = 10.;
var mousePositions = [];
var color = [];
var vertexPositions = [];
var buffer;

var uniformTranslation;
var globalTranslation = [0, 0];

var uniformZoom;
var globalZoom = 1;

var uniformRotation;
var globalRotation = 0;

var uniformPerspective;

var globalFov = 1;
var matrix;
var uniformLocationMatrix;

function initContext() {
    canvas = document.getElementById('dawin-webgl');
    gl = canvas.getContext('webgl');
    if (!gl) {
        console.log('ERREUR : echec chargement du contexte');
        return;
    }
    gl.clearColor(0.2, 0.2, 0.2, 1.0);

    matrix = mat4.create();
    mat4.perspective(matrix, 90, 1, 0, 1);
}

//Initialisation des shaders et du program
function initShaders() {
    var fragmentSource = loadText('fragment.glsl');
    var vertexSource = loadText('vertex.glsl');

    var fragment = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragment, fragmentSource);
    gl.compileShader(fragment);

    var vertex = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertex, vertexSource);
    gl.compileShader(vertex);

    gl.getShaderParameter(fragment, gl.COMPILE_STATUS);
    gl.getShaderParameter(vertex, gl.COMPILE_STATUS);

    if (!gl.getShaderParameter(fragment, gl.COMPILE_STATUS)) {
        console.log(gl.getShaderInfoLog(fragment));
    }

    if (!gl.getShaderParameter(vertex, gl.COMPILE_STATUS)) {
        console.log(gl.getShaderInfoLog(vertex));
    }

    program = gl.createProgram();
    gl.attachShader(program, fragment);
    gl.attachShader(program, vertex);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.log("Could not initialise shaders");
    }
    gl.useProgram(program);
}



//Evenement souris
function initEvents() {
    canvas.onclick = function (e) {
        //changement de repere pour les coordonnees de souris
        var x = (e.pageX / canvas.width) * 2.0 - 1.0;
        var y = ((canvas.height - e.pageY) / canvas.height) * 2.0 - 1.0;
        mousePositions.push(x);
        mousePositions.push(y);

        for (let c = 0; c < 3; c++) {
            color.push(Math.random());
        }
        color.push(1.0);

        refreshBuffers();
    }

    document.onkeypress = event => {
        let accel = .1;
        let zoomSpeed = .1;
        switch (event.code) {
            case 'KeyW':
                globalTranslation[1] += accel;
                break;
            case 'KeyA':
                globalTranslation[0] -= accel;
                break;
            case 'KeyS':
                globalTranslation[1] -= accel;
                break;
            case 'KeyD':
                globalTranslation[0] += accel;
                break;
            case 'KeyX':
                globalZoom += zoomSpeed;
                break;
            case 'KeyC':
                globalZoom -= zoomSpeed;
                break;
            case 'KeyQ':
                globalRotation += Math.PI / 20;
                break;
            case 'KeyE':
                globalRotation -= Math.PI / 20;
                break;
            default:
                break;
        }
        refreshBuffers();
        draw();
    }
}

//Fonction initialisant les attributs pour l'affichage (position et taille)
function initAttributes() {

    uniformTranslation = gl.getUniformLocation(program, "translation");
    uniformZoom = gl.getUniformLocation(program, "zoom");
    uniformRotation = gl.getUniformLocation(program, "rotation");
    uniformPerspective = gl.getUniformLocation(program, "perspective");

    uniformLocationMatrix = gl.getUniformLocation(program, "matrix");

    gl.uniformMatrix4fv(uniformLocationMatrix, false, matrix);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mousePositions), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(color), gl.STATIC_DRAW);
}

function setCube() {
    vertexPositions = [
        -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0,
        -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0, 1.0, -1.0, 1.0, -1.0, -1.0, 1.0,
        1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0,
        1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, -1.0
    ];
}


//Initialisation des buffers
function initBuffers() {
    buffer = gl.createBuffer();
    colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
}

//Mise a jour des buffers : necessaire car les coordonnees des points sont ajoutees a chaque clic
function refreshBuffers() {
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mousePositions), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(color), gl.STATIC_DRAW);

    draw();
}

//Fonction permettant le dessin dans le canvas
function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.uniform2f(uniformTranslation, globalTranslation[0], globalTranslation[1]);
    gl.uniform2f(uniformZoom, globalZoom, globalZoom);
    gl.uniform1f(uniformRotation, globalRotation);

    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    var attribCol = gl.getAttribLocation(program, "color");
    gl.enableVertexAttribArray(attribCol);
    gl.vertexAttribPointer(attribCol, 4, gl.FLOAT, true, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    attribPos = gl.getAttribLocation(program, "position");
    gl.enableVertexAttribArray(attribPos);
    gl.vertexAttribPointer(attribPos, 2, gl.FLOAT, true, 0, 0);
    gl.drawArrays(gl.TRIANGLES, 0, vertexPositions.length / 3);
}


function main() {
    initContext();
    initShaders();
    initBuffers();
    initAttributes();
    initEvents();
    draw();
}
